# Programmiertechnik

## Eigenschaften von Algorithmen

- **Eindeutigkeit**
 Liefert eindeutig Beschreibung zur Lösung eines gegebenen Problems.
 Enthält keine Widersprüchen Anweisung.
- **Parametrisierbarkeit**
 Keine Spezialisierung, sondern Anpassung auf eine "Klasse" von 
 Problemen. Z.B. nicht nur die Summe für zwei bestimmte Zahlen bilden,
 sondern für belibige Zahlen. Diese Zahlen sind die Parameter.
- **Terminierung**
 Ein Algorithmus ist nach einer endlichen Anzahl von Schritten beendet.
- **Ausfürbarkeit**
 Jede Anweisung muss ausfürbar sein.
- **Determinierheit**
 Er liefert unter gleichen Bedingungen bei mehrfacher Ausfürung stets
 das gleiche Ergebnis.
## Qualitätsmerkmale eines Algorithmus
- **Aufgabe**:
 Überlegt, welche Anforderungen
 in der Praxis relevant sind, um 
 einen Algorithmus als "gut" zu
 bezeichnen.

- **Effizienz**
 Er soll so schnell wie möglich sein und wenig Ressourcen wie
 nötig verbrauchen.
- **Wiederverwentparkeit**
 Er soll auch in anderen Zusammenhängen einsetzbar sein.
- **Zuverlässigkeit**
 Probleme sind korrekt und vollständig zu lösen.
- **Erweiterbarkeit**
 Der Aufwan zur Anpassung an geänderte Anforderungen soll so gering
 wie möglich sein.
- **Portabilität**
 Er soll nicht nur auf bestimmten Systemen Funktionieren, sondern auf
 belibigen Computertypen einsetztbar sein.
## Darstelung von Algorithmen
## Flussdiagramme
Ein Flussdiagramm enthält ebenfalls Anweisungen, Verzweigungen und Schleifen
- **Ein/Ausgaben Parralelogramm**
 Hier werden entweder Daten entgegengenommen, diese können
 vom Benutzer eingegeben oder von anderen Prozesen, Dateien
 o.ä übernommen werden. Oder es werden Daten ausgegeben,
 meist am Bildschirm oder an andere Prozese
- **Anweisung Rechteck**
- **Anfang/Ende Elipse**
 Beschreibt den Einstieg/Aufruf des Programms und mögliche
 Ausstiege/Ende
- **Verzweigung Raute**
 Enthälteine Bedingug, die mit "ja" oder "nein" beantwortet 
 werden kann