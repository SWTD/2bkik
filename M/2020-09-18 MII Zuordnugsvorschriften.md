## MII 18.09.2020

# Funktionen und Zuordnungsvorschriften

Ein Beispiel einer Zuordnungsvorschrifft einer Funktion
$$f: x \rightarrow  x^3$$
In Worten: Die Funktion $f$ ordnet jeder Zahl $x$ ihre dritte Potenz $x^3$ zu.

$x^3$ nennt man auch den Funktionsterm und $f(x)=x^3$ die Funktiongleichung.
In der Mittelstufe schreibt man $y$ anstelle von $f(x)$, 
das heißt $y=x^3$ lautet bis zur 10. Klasse 
die Funktiongleichung.

$f(x)$ ist der sogenante Funktionwert an der Stelle $x$.

Zum Beispiel ist $f(2)$ der Funktionswert an der Stelle $2$, nämlich $8$.

Trägt man alle Punkte, die die sogenante Punkteprobe bestehen,
das heißt deren y-Koordinaten die Funktionswerte an den
jeweiligen Stelle (x-Koordinaten) sind,in ein Koordinatensystem ein, so erhält man den 
Funktionsgraphen der Funktion. Manchmal sagt man auch Schaubild dazu.

